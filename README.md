# Apprentissage autonome  / Intelligence Artificielle
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mikeleyeti%2Fapprentissage_autonome/master)

## Ressources
- [MOOC - L'Intelligence Artificielle... avec intelligence !](https://www.fun-mooc.fr/fr/cours/lintelligence-artificielle-avec-intelligence/)
- [L’Intelligence Artificielle pour TOUS !](https://www.fun-mooc.fr/fr/cours/lintelligence-artificielle-pour-tous/)

## Avancées les plus médiatiques
- ELIZA (1964) *(Lié au test de Turing)*
- Deep Blue (1997)
- Assistants vocaux
- Alpha Go
- Mécanismes intelligents à bord des voitures
- Caisse automatique via detection des achats automatique
- Génération automatique de sous-titre / Traduction automatique (youtube)
- 

## L'I.A. dans la culture/société
- 2001, l'odyssée de l'espace
- Terminator
- Her
- Cassandres : Elon Musk, Stephen Hawking, Bill Gates

## Histoire [Wiki](https://fr.wikipedia.org/wiki/Histoire_de_l%27intelligence_artificielle) et [vidéo Nicolas Rougier](https://lms.fun-mooc.fr/courses/course-v1:inria+41021+session01/courseware/e748a8c9281b43e2962f9e4cabfb3da5/2dbb2c6752684b3588011e00e9760fd6/1?activate_block_id=block-v1%3Ainria%2B41021%2Bsession01%2Btype%40vertical%2Bblock%40fb74be0b63a2427fbfa76d2e822a0a76)
- Naissance (1956) : Conférence Darthmouth
  - logicien théorique ou logicien théoricien - C'est un programme qui a été capable de démontrer 38 des théorèmes des Principa Mathematica
- 2 Approches 
  - Symbolique : créer un esprit artificiel, l'idée fondatrice est que le cerveau est une machine à traiter des symboles
  - Numérique : modéliser le cerveau
- Age d'or IA (1956 - 1974) - Courant symbolique prédominant
- Fin age d'or - "rejet" du courant symbolique. Début de la montée en puissance du courant numérique (réseaux de neurones)
- 1er hivers de l'I.A. (1974 - 1980) : l'intelligence artificielle subit critiques et revers budgétaires, car les chercheurs en intelligence artificielle n'ont pas une vision claire des difficultés des problèmes auxquels ils sont confrontés. Moins de fonds sont alloués car les attentes des financeurs sont déçues. Attente d'une intelligence générale non atteinte
- Le boom (1980 - 1987) : les systèmes experts
- 2eme hiver de l'I.A. (1987 - 1995) : Trop de hype autour des systèmes experts et la déception fait chuter les investissements
- De nombreux progrès depuis 1995, mais toujours pas d'intelligence générale (et tant mieux ?). Les chercheurs en intelligence artificielle développent et utilisent des outils mathématiques sophistiqués comme jamais auparavant. Ils prennent conscience que de nombreux problèmes que l'intelligence artificielle doit résoudre ont déjà été traités dans d'autres domaines comme les mathématiques, l'économie ou la recherche opérationnelle. *Mauvaise réputation ?* "Les scientifiques en informatique et les ingénieurs logiciel ont évité l'expression 'intelligence artificielle' par crainte d'être considérés comme de doux illuminés rêveurs"
- Gros succès de la période : Deep Blue en 1997 puis jeux de go en 2017
- **Des** intelligence**s** artificielle**s** : le problème attenant, c'est qu'il faut à un moment définir ce qu'on va entendre par "intelligence". C'est un peu là où les chercheurs ont péché à l'origine. C'est qu'on ne va pas avoir une intelligence, mais des intelligences. C'est pour ça qu'on va préférer parler d'intelligences artificielles au pluriel, on va même enlever le terme d'intelligence artificielle au profit d'autres domaines qui peuvent être, par exemple, l'apprentissage machine, qui peuvent être les réseaux de neurones artificiels, qui peuvent être les systèmes experts, qui peuvent être encore tout un tas d'autres domaines. 
- Les raisons du boom actuel :
  - Amélioration des performances des ordinateurs (loi de moore)
  - optimisation des algorithmes
  - **Très grande quantité de données disponibles**

## Exemple d'algorithmes d'I.A.
- [Perceptron](https://fr.wikipedia.org/wiki/Perceptron) (1957) inventé par Frank Roseblatt

## Exemples d'utilisation d'algorithme d'I.A.
- Reconnaissance d'image pour le diagnostique médical
- Conduite autonome
- Traduction
- Reconnaissance de la parole
- Reconnaissance de la langue, traduction instantanée
- Jeux (échec, go, starcraft, LOL, jeux Atari ...)
- Reconnaissance visage
- Description automatique d'une image (google)
- Recommandation de contenu
- Robotique (Chalange DARPA)
- Deepfake


## Synthèse
- la définition de l’IA se situe entre mythes, fantasmes, innovations scientifiques et techniques.
- Elles (les I.A.**s**) ne peuvent faire que la tâche pour laquelle elles ont été programmées ou paramétrées Un programme qui bat le champion du monde d’échecs ne sait pas reconnaître un canard !!